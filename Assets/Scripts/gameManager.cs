using System;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameManager : MonoBehaviour
{
  public static gameManager Instance;

  float _timerForAllGame = 30;
  float _timerPerMusic = 30;

  [SerializeField] gameData curData;
  [SerializeField] gameData _70BpmData;
  [SerializeField] gameData _80BpmData;
  [SerializeField] gameData _90BpmData;
  [SerializeField] gameData _100BpmData;
  [SerializeField] gameData _110BpmData;
  [SerializeField] gameData _120BpmData;
  [SerializeField] gameData _130BpmData;
  [SerializeField] gameData _noneData;

  public event Action endOfRund = delegate { };

  [SerializeField] BPMTestManager bPMTestManager;
  [SerializeField] float TimePerMusic = 30;
  [SerializeField] Text _accuracyText;
  [SerializeField] Text _timerText;

  [SerializeField] GameObject chert;
  [SerializeField] GameObject startSessionBtn;
  MusicManager _musicManager;

  int _myEnumMemberCount;
  int index = 0;

  BPM _BPM;

  int currentMode = -1;
  int currentSensRound = 0;

  string userName = "User";

  public bool firstShot = false;
  bool sessionRunning = false;
  bool isSensativityMode = true;

  private void Awake()
  {
    Instance = this;
    chert.SetActive(false);
  }

  void Start()
  {
    _musicManager = MusicManager.inc;
    //bPMTestManager.StartTest();
  }

  public void SetUserName(string name)
  {
    userName = name;
  }

  public void SetMode()
  {
    sessionRunning = true;
    if (currentMode == 140)
    {
      SceneManager.LoadSceneAsync("MainMenu");
      return;
    }
    if (currentMode >= 0)
      SumUpSession();
    //currentMode = mode;
    _timerForAllGame = 30;
    chert.SetActive(false);
    firstShot = false;
    Cursor.visible = false;
    StartCoroutine(SetModeRoutine());
    Time.timeScale = 1;

  }

  public void AdvanceSession()
  {
    SetMode();
  }


  IEnumerator SetModeRoutine()
  {
    yield return null;
    if (currentMode > 1)
      GetComponent<AudioSource>().clip = (Resources.Load<AudioClip>($"{currentMode}BPM"));
    switch (currentMode)
    {
      case 0:
        break;
      case 1:
        break;
      case 70:
        break;
      case 80:
        break;
      case 90:
        break;
      case 100:
        break;
      case 110:
        break;
      case 120:
        break;
      case 130:
        break;
    }
  }

  private void Update()
  {
    if (!isSensativityMode)
    {
      if (firstShot)
      {
        //target.instance.Clearlist();
        if (!GetComponent<AudioSource>().isPlaying)
          GetComponent<AudioSource>().Play();
        _timerForAllGame -= Time.deltaTime;
        _timerPerMusic -= Time.deltaTime;

        if (_timerForAllGame <= 0 && currentMode <= 130 && sessionRunning)
        {
          sessionRunning = false;
          //Debug.Log(currentMode);
          Time.timeScale = 0;
          GetComponent<AudioSource>().Stop();
          if (currentSensRound < 2)
            currentSensRound++;
          else if (currentMode == 0)
            currentMode = 70;
          else
            currentMode += 10;
          if (currentMode <= 130)
          {
            Cursor.visible = true;
            chert.SetActive(true);
          }
        }
      }


      /* if (_timerPerMusic <= 0 && index < _myEnumMemberCount)
       {
         _musicManager._bpm = BPM._80BPM;
         index++;
         _timerPerMusic = TimePerMusic;
         endOfRund();

       }*/
    }

    StringBuilder builder = new StringBuilder();
    string rgb = _timerForAllGame > 10 ? "green" : "red";
    builder.AppendLine($"<color={rgb}>{(int)_timerForAllGame}</color>");
    _timerText.text = builder.ToString();
    StaeOfInfo();
  }

  public void StaeOfInfo()
  {
    return;
    switch (_musicManager._bpm)
    {
      case BPM._70BPM:
        curData = _70BpmData;
        break;
      case BPM._80BPM:
        curData = _80BpmData;
        break;
      case BPM._90BPM:
        curData = _90BpmData;
        break;
      case BPM._100BPM:
        curData = _100BpmData;
        break;
      case BPM._110BPM:
        curData = _110BpmData;
        break;
      case BPM._120BPM:
        curData = _120BpmData;
        break;
      case BPM._130BPM:
        curData = _130BpmData;
        break;
      case BPM.None:
        curData = _noneData;
        break;
      default:
        break;
    }
  }

  public void ShotHit()
  {
    if (currentMode >= 0)
    {
      curData._shotHit++;
      AccuracyTest();
    }
  }

  public void ShotMiss()
  {
    if (currentMode >= 0)
    {
      curData._shotMiss++;
      AccuracyTest();
    }
  }

  public void AccuracyTest()
  {
    var tergetOverAll = curData._shotHit + curData._shotMiss;
    curData._accuracy = curData._shotHit / tergetOverAll;
    curData._accuracy *= 100;
    string a = string.Format("{0:0.00}", curData._accuracy);
    _accuracyText.text = "Accuracy: " + a + "%";

    Debug.Log(tergetOverAll);
  }

  public float GetAccuracyRate()
  {
    return curData._accuracy;
  }

  public float getNamOfTarget()
  {
    return curData._shotHit;
  }

  void SumUpSession()
  {
    float accuracy = curData._accuracy;
    float targetOverall = curData._shotMiss + curData._shotHit;
    float ttk = target.instance.ttkC();
    string path = Path.Combine(Application.dataPath, "StreamingAssets", $"{userName + DateTime.Now.ToString("MMMM dd")}.txt");
    string txtToWrite = $"Accuracy : {curData._accuracy} TTK: {ttk} BPM: {currentMode} \n";
    if (!File.Exists(path))
    {
      File.WriteAllText(path, txtToWrite);
    }
    else
    {
      File.AppendAllText(path, txtToWrite);
    }
    curData.ResetData();
    target.instance.Clearlist();
    Debug.Log("ttk :" + ttk);

  }

  public void StartSession()
  {
    firstShot = false;
    chert.SetActive(false);
    startSessionBtn.SetActive(false);
    isSensativityMode = false;
    currentMode = 0;
  }
}
