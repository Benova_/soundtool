using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSound : MonoBehaviour
{

    gun _gun;

    AudioSource _audioSource;

   [SerializeField] AudioClip _audioClip;
    void Start()
    {
        _gun = GetComponent<gun>();
        _gun.onFire += Gun_onFire;

        _audioSource = GetComponent<AudioSource>();
    }

    private void Gun_onFire()
    {
        _audioSource.PlayOneShot(_audioClip);
    }

}
