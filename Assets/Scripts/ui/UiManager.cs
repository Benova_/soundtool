using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    [Header("Accuracy")]
    [SerializeField] Image AccuracyFillingImage;
    [SerializeField] Text  AccuracyFillingText;

    [Header("NumberOfTargets")]
    [SerializeField] Image NumberOfTargetsHitImage;
    [SerializeField] Text NumberOfTargetsHitText;

    [Header("ttk")]
    [SerializeField] Image TTK_Image;
    [SerializeField] Text TTk_Text;


    private void OnEnable()
    {
        AccuracyFillingImage.fillAmount = gameManager.Instance.GetAccuracyRate();
        AccuracyFillingText.text = gameManager.Instance.GetAccuracyRate().ToString();
        NumberOfTargetsHitImage.fillAmount = gameManager.Instance.getNamOfTarget()/1500;
        NumberOfTargetsHitText.text = gameManager.Instance.getNamOfTarget().ToString();

        TTK_Image.fillAmount = target.instance.ttkC();
        TTk_Text.text = target.instance.ttkC().ToString();
    }

 

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
