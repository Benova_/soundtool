using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour
{
    public static MusicManager inc;
    public BPM _bpm;
    AudioSource _audioSource;
    bool isItOn;
    [SerializeField]
    AudioClip _70BPM, _80BPM, _90BPM,_100BPM, _110BPM, _120BPM, _130BPM;
    private void Awake()
    {
        inc = this;
        _audioSource = GetComponent<AudioSource>();

    }
    private void Start()
    {
        
        gameManager.Instance.endOfRund += Ins_endOfRund;
    }
    private void Ins_endOfRund()
    {
        PutMusic();
    }



    void PutMusic()
    {
        switch (_bpm)
        {
            case BPM._70BPM:
                if (!isItOn)_audioSource.PlayOneShot(_70BPM);
                isItOn = true;
                break;
            case BPM._80BPM:
                if (!isItOn) _audioSource.PlayOneShot(_80BPM);
                isItOn = true;
                break;
            case BPM._90BPM:
                if (!isItOn) _audioSource.PlayOneShot(_90BPM);
                isItOn = true;
                break;
            case BPM._100BPM:
                if (!isItOn) _audioSource.PlayOneShot(_100BPM);
                isItOn = true;
                break;
            case BPM._110BPM:
                if (!isItOn) _audioSource.PlayOneShot(_110BPM);
                isItOn = true;
                break;
            case BPM._120BPM:
                if (!isItOn) _audioSource.PlayOneShot(_120BPM);
                isItOn = true;
                break;
            case BPM._130BPM:
                if (!isItOn) _audioSource.PlayOneShot(_130BPM);
                isItOn = true;
                break;
            default:
                _audioSource.Stop();
                break;
        }
    }
}

public enum BPM
{
    _70BPM, _80BPM, _90BPM,
    _100BPM, _110BPM, _120BPM, _130BPM,None
} 
