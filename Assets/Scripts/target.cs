using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class target : MonoBehaviour
{
    public Transform[] RandomPoint;
    public static target instance;

    [SerializeField]AudioClip _hitMarker;
    [SerializeField] AudioSource _audioSource;

    float timeBetweenShots;
    List<float> timeBetweenShotsList = new List<float>();
    float m = 0;

    float Nam;
    private void Awake()
    {
        instance = this;
        
    }
    private void Update()
    {
        timeBetweenShots += Time.deltaTime;
    }
    public void getHit()
    {
        var randomPoint = Random.Range(0, RandomPoint.Length);
        var addTime = timeBetweenShots;
        timeBetweenShotsList.Add(addTime);
        timeBetweenShots = 0;
        if (randomPoint == Nam)
        {
            randomPoint = Random.Range(0, RandomPoint.Length);
        }
        transform.position = RandomPoint[randomPoint].position;
        _audioSource.PlayOneShot(_hitMarker);
        Nam = randomPoint;
    }

    public float ttkC()
    {
        m = 0;
        foreach (var item in timeBetweenShotsList)
        {
            m += item;
        }
            //Debug.Log(m);
        float average = m / timeBetweenShotsList.Count;
        Debug.Log(average);
        return average;
    }

  public void Clearlist()
  {
    timeBetweenShotsList.Clear();
    timeBetweenShots = 0;
  }
}
