using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseLookMovement : MonoBehaviour
{
    private float _rotationSpeed =1000;
    private float mouseLookSenstivity =1000;

    float mouseHorizontal;
    float mouseVertical;

    float xRot;

   [SerializeField] Transform PlayerBody;

    void Start()
    {
        
    }


    void Update()
    {
        if (Cursor.visible == true) return;
         mouseHorizontal = Input.GetAxis("Mouse X") * Time.deltaTime * mouseSenstivity.MouseSenstivity;
         mouseVertical = Input.GetAxis("Mouse Y") * mouseSenstivity.MouseSenstivity * Time.deltaTime; ;
      

         xRot -= mouseVertical;
         xRot = Mathf.Clamp(xRot, -90, 90);
         transform.localRotation = Quaternion.Euler(xRot, 0, 0);
         PlayerBody.Rotate( mouseHorizontal * Vector3.up);
            

    }

    private void FixedUpdate()
    {

    }
}
