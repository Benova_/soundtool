using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMuzzleFlseh : MonoBehaviour
{

    gun Gun;
    [SerializeField] GameObject _muzzleFlseh;

    void Start()
    {
        Gun = GetComponent<gun>();
        Gun.onFire += Gun_onFire;
    }

    private void Gun_onFire()
    {
        StartCoroutine(MuzzleFlseh());
    }

    private IEnumerator MuzzleFlseh()
    {
        _muzzleFlseh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        _muzzleFlseh.SetActive(false);
    }
}
