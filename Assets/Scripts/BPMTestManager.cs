using System.Collections;
using UnityEngine;

public class BPMTestManager : MonoBehaviour
{
  const int INIT_BPM_VALUE = 70;

  //[SerializeField] ScoreManager rangeScore;
  [SerializeField] AudioClip initBPM;
  [SerializeField] AudioSource audioSource;

  [SerializeField] int lastBPM;
  [SerializeField] int currentBPM;

  float lastScore = 0;
  [SerializeField] int currentIntervalIndex = 0;

  public void StartTest()
  {
    TestBPMSession(INIT_BPM_VALUE);
  }

  void TestBPMSession(int requiredBpm)
  {
    lastBPM = currentBPM;
    currentBPM = requiredBpm;
    audioSource.PlayOneShot(Resources.Load<AudioClip>($"{requiredBpm}BPM"));
    FindObjectOfType<gameManager>().StartSession();
    StartCoroutine(ManageSession(10));
  }

  IEnumerator ManageSession(int secondsPerSession)
  {
    yield return new WaitForSeconds(secondsPerSession);
    audioSource.Stop();
    SetRangeScore(FindObjectOfType<gameManager>().GetAccuracyRate());
  }

  void SetRangeScore(float newScore)
  {
    ManipulateRangeScore(newScore);
  }

  void ManipulateRangeScore(float newScore)
  {
    Debug.Log($"currentBPM:{currentBPM} lastBPM:{lastBPM} newScore:{newScore} lastScore:{lastScore}");
    if (currentIntervalIndex == 0)
      TestBPMSession(currentBPM + 10);
    else
    {
      if (currentBPM > lastBPM && newScore > lastScore || currentBPM < lastBPM && newScore < lastScore)
        TestBPMSession(currentBPM + 10);
      else if (currentBPM < lastBPM && newScore > lastScore || currentBPM > lastBPM && newScore < lastScore)
        TestBPMSession(currentBPM - 10);
      else
        StopTest();
    }
    currentIntervalIndex++;
    lastScore = newScore;
  }

  void StopTest()
  {
    Debug.Log("BPM: " + lastBPM);
  }
}
