using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAnimation : MonoBehaviour
{
    Animator _animator;
    gun Gun;
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            _animator = transform.GetChild(i).GetComponentInChildren<Animator>();
        }
        //_animator = GetComponentInChildren<Animator>();
        Gun = GetComponent<gun>();
        Gun.onFire += WeaponAnimation_onFire;
    }

    private void WeaponAnimation_onFire()
    {
        _animator.SetTrigger("Shot");

    }


}
